# Youtube Thumbnail Generator

![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")

![Alt text](thumbnail.png?raw=true "Thumbnail")

Youtube Thumbnail Generator without annoying ads.

## Demo

You can view it on [GitLab pages](https://ethicist.gitlab.io/youthumb) and [here](https://youthumb.surge.sh/)

## Contributing

Please feel free to submit pull requests.
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
