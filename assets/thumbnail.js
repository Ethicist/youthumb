
var base_url = 'http://img.youtube.com/vi/';

var heading = new Array (
  "Max resolution (1080p and higher):",
  "High resolution (720p):",
  "Standard (480p):",
  "Medium (360p):",
  "Small (144p):"
);
var image = new Array (
  "maxresdefault.jpg",
  "sddefault.jpg",
  "hqdefault.jpg",
  "mqdefault.jpg",
  "default.jpg"
);

function focus() {
  document.getElementsByName('url_input')[0].focus();
}

function insert_thumbnails() {
  var content = '';
  for (i=0; i<5; i++) {
    var thumbnail = base_url + document.getElementById('hidden_video_id').value + '/' + image[i];
    content += '<div class="list-group"><div class="list-group-item heading">' + heading[i] + '</div><div class="list-group-item content"><a href="' + thumbnail + '" target="_blank"><img src="' + thumbnail + '" class="img-responsive"></a><br><div class="input-group"><span class="input-group-btn"><a href="' +thumbnail + '" download="' + image[i] + '"><button class="btn btn-dark">DOWNLOAD</button></a></span><span class="input-group-addon spacer"></span><span class="input-group-addon">URL:</span><input type="text"  value="' + thumbnail + '" class="form-control" readonly="readonly" onclick="this.select()"></div></div></div>' + ((i==4) ? '<br>' : '');
  }
  document.getElementsByClassName("thumbnail-container")[0].style.display = "block";
  document.getElementsByClassName("thumbnail-container")[0].innerHTML = content + '';
}


function update_placeholder() {
  var length = 11,
  dict = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
  link = "https://www.youtube.com/watch?v=";
  for (var i = 0, n = dict.length; i < length; ++i) {
    link += dict.charAt(Math.floor(Math.random() * n));
  }
  document.getElementsByName("url_input")[0].value = "";
  document.getElementsByName("url_input")[0].placeholder = link;
}


function parse_url() {
  if (document.getElementsByName('url_input')[0].value == "") {
    alert('You need to enter YouTube video URL first.');
    return false;
  }
  url = document.getElementsByName('url_input')[0].value;
  var reg_exp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  var match = url.match(reg_exp);
  if (match && match[7].length == 11) {
    document.getElementById('hidden_video_id').value = match[7];
    insert_thumbnails();
    return true;
  } else {
    alert('Invalid URL. Copy and paste URL and try again.');
    return false;
  }
}

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./service-worker.js').then(function(reg){
  }).catch(function(err) {
  });
}